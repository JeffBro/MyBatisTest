import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import test.Blog;
import test.BlogMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Demo {
    public static void main(String[] args) throws IOException {
    String resource = "mybatis-config.xml";
    InputStream inputStream = Resources.getResourceAsStream(resource);
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

    List<String> ids = new ArrayList<>();
    ids.add("1");
    ids.add("3");
    ids.add("4");

    Map<String,Object> map = new HashMap<>();
    map.put("id",1);
    map.put("title","ABC");

    int[] blogIds = new int[]{1,3};

    List<String> insertTitle = new ArrayList<>();
    insertTitle.add("X");
    insertTitle.add("Y");
    insertTitle.add("Z");

    try (SqlSession session = sqlSessionFactory.openSession()) {
        BlogMapper mapper = session.getMapper(BlogMapper.class);

        //单参数
        Blog blog = mapper.selectBlog(1);

        //list单参数
        List<Blog> blogList = mapper.selectBlogList(ids);

        //array单参数
        List<Blog> blogListByArray = mapper.selectBlogByArray(blogIds);
        for(Blog b1 : blogListByArray){
            System.out.println("来了数组：" + b1.getTitle());
        }

        //map
        List<Blog> blogListMap = mapper.selectBlogByMap(map);
        for(Blog b : blogListMap){
            System.out.println("来了 老弟："  + b.getTitle());
        }

        mapper.insertBlog(insertTitle);
        session.commit();
        session.close();
    }catch (Exception e){
        e.printStackTrace();
    }
    }
}
