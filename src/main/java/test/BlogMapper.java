package test;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BlogMapper {

    Blog selectBlog(Integer id);
    List<Blog> selectBlogList(List<String> blogIds);
    List<Blog> selectBlogByMap(@Param("mapByIdTitle") Map<String,Object> map);
    List<Blog> selectBlogByArray(int[] ids);
    void insertBlog(List<String> titles);
}
